import static org.junit.Assert.*;

import org.junit.Test;

public class GameSquareTest {

	@Test
	public void initializedProperly() {
		// Create an empty game square
		int row = 0;
		int col = 0;
		GameSquare gameSquare = new GameSquare(row, col);
		
		// Row and Col position should be correct
		assertTrue(gameSquare.getRow() == row);
		assertTrue(gameSquare.getCol() == col);
		
		// Ensure that is empty
		assertFalse(gameSquare.isOccupied());
		assertTrue(gameSquare.available());
		
		// It does not belong to some random Player
		Player xPlayer = new Player("X");
		assertFalse(gameSquare.ownedByPlayer(xPlayer));
	}
	
	@Test
	public void claimedByPlayer() {
		int row = 0;
		int col = 0;
		GameSquare gameSquare = new GameSquare(row, col);
		
		// Create two players, one who will claim the game square
		Player xPlayer = new Player("X");
		Player oPLayer = new Player("O");
		
		// xPlayer claims the square
		try {
			gameSquare.claim(xPlayer);
		} catch (UnavailableGameSquareException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		// Ensure that the GameSquare is properly claimed
		assertTrue(gameSquare.isOccupied());
		assertFalse(gameSquare.available());
		assertTrue(gameSquare.ownedByPlayer(xPlayer));
		assertFalse(gameSquare.ownedByPlayer(oPLayer));
	}
	
	@Test
	public void clearedProperly() {
		int row = 0;
		int col = 0;
		GameSquare gameSquare = new GameSquare(row, col);
		
		// Create two players, one who will claim the game square
		Player xPlayer = new Player("X");
		Player oPLayer = new Player("O");
		
		// xPlayer claims the square
		try {
			gameSquare.claim(xPlayer);
		} catch (UnavailableGameSquareException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		// Now, clear the GameSquare
		gameSquare.clear();
		
		// Ensure that it is now empty
		assertFalse(gameSquare.isOccupied());
		assertTrue(gameSquare.available());
		assertFalse(gameSquare.ownedByPlayer(xPlayer));
		assertFalse(gameSquare.ownedByPlayer(oPLayer));
	}
	
	@Test
	public void cantBeClaimedTwice() {
		int row = 0;
		int col = 0;
		GameSquare gameSquare = new GameSquare(row, col);
		
		// Create two players, one who will claim the game square
		Player xPlayer = new Player("X");
		Player oPlayer = new Player("O");
		
		// xPlayer claims the square
		try {
			gameSquare.claim(xPlayer);
		} catch (UnavailableGameSquareException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		// Now, attempt to claim the game square again. This time 
		// an exception should be thrown.
		try {
			gameSquare.claim(oPlayer);
			fail("Should have thrown an excpetion!"); // should not reach this point
		} catch (UnavailableGameSquareException e) {
			// Success! 
			assertEquals("Attempted to claim GameSquare that was previously occupied!", e.getMessage());
		}
		
		// Finally, ensure that the xPlayer still owns this GameSquare, and that oPlayer does not
		assertTrue(gameSquare.ownedByPlayer(xPlayer));
		assertFalse(gameSquare.ownedByPlayer(oPlayer));
	}
}
