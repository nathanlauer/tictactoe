import java.util.Random;
import java.util.Scanner;

/** This class coordinates the activities involved with playing a single game of
 * Tic Tac Toe. This Involves the process of creating a new game, prompting the
 * players for their moves, and then indicating the result of the game.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class TicTacToe implements Game {
	private Player xPlayer;
	private Player oPlayer;
	private Player[] allPlayers;
	private Player winner;
	private Player loser;
	private Random random;
	private Scanner scanner;
	private TicTacToeGameBoard board;
	
	/** Standard constructor, passing in the two players to the game
	 * 
	 * @param xPlayer
	 * @param oPlayer
	 */
	public TicTacToe(Player xPlayer, Player oPlayer, Scanner scanner) {
		this.xPlayer = xPlayer;
		this.oPlayer = oPlayer;
		this.winner = null;
		this.loser = null;
		this.allPlayers = new Player[] {xPlayer, oPlayer};
		this.random = new Random();
		this.scanner = scanner;
		this.board = new TicTacToeGameBoard();
	}
	
	/** The "main" method of this class, this method actually runs the game.
	 * It goes through the following steps:
	 * 1) Prints a welcome message to stdout
	 * 2) Shows an empty board, with positions.
	 * 3) Picks a random player to go first, and prompts them to enter their move
	 * 4) Alternates between players making moves
	 * 5) Ends when one player has won, or a stalemate has been reached.
	 * 6) Generates a GameResult, and prints it to stdout
	 * 
	 */
	public GameResult play() {
		Player first = this.random.nextBoolean() ? this.xPlayer : this.oPlayer;
		boolean gameFinished = false;
		boolean gameWasWon = false;
		
		System.out.println("New Game! Player " + first.getGameIdentifier() + " will go first. Here is the board, numbered with available squares:");
		this.printBoardAvailableSpots();
		
		Player current = first;
		while(!gameFinished) {
			// Prompt the player for their move
			int gameSquareNumber = this.getUserInput(current, scanner);
			
			// Attempt to take the square indicated by the user
			int row = this.getRowFromNumber(gameSquareNumber);
			int col = this.getColFromNumber(gameSquareNumber);
			boolean madeValidMove = false;
			while(!madeValidMove) {
				try {
					this.board.claimGameSquare(row, col, current);
					madeValidMove = true;
				} catch (IndexOutOfBoundsException e) {
					System.out.println(e.getMessage());
					System.out.println("Error! Row " + row + ", col " + col + " will not work.");
				} catch (UnavailableGameSquareException e) {
					System.out.println("Invalid move! Sqaure " + gameSquareNumber + " is already occupied. Please try again");
					gameSquareNumber = this.getUserInput(current, scanner);
					row = this.getRowFromNumber(gameSquareNumber);
					col = this.getColFromNumber(gameSquareNumber);
				}
			}
			
			// Display the board
			this.printBoardPosition();
			this.printBoardAvailableSpots();
			
			// Check if the game has been won
			if(this.board.winningPositionForPlayer(this.xPlayer)) {
				this.printCongratsToWinner(this.xPlayer);
				this.winner = this.xPlayer;
				this.loser = this.oPlayer;
				gameFinished = true;
				gameWasWon = true;
			}
			
			if(this.board.winningPositionForPlayer(this.oPlayer)) {
				this.printCongratsToWinner(this.oPlayer);
				this.winner = this.oPlayer;
				this.loser = this.xPlayer;
				gameFinished = true;
				gameWasWon = true;
			}
			
			// Check for a tie
			if(this.board.positionIsTied(this.allPlayers)) {
				System.out.println("Game over, it's a tie.");
				gameFinished = true;
				gameWasWon = false;
			}
			
			// Swap players
			if(!gameFinished) {
				if(current.equals(this.xPlayer)) {
					current = this.oPlayer;
				} else {
					current = this.xPlayer;
				}
			}
		}
		
		if(gameWasWon) {
			return new GameWonByPlayer(this.winner, this.loser);
		} else {
			return new GameTied();
		}
	}
	
	private void printCongratsToWinner(Player winner) {
		System.out.println("Congratulations Player " + winner.getGameIdentifier() + "! You have won the game!");
	}
	
	/** Prompts the user for their move -- a number between 1 and 9 -- and 
	 * continues to do so until the user has entered a valid number.
	 * 
	 * @param current
	 * @param scanner
	 * @return
	 */
	private int getUserInput(Player current, Scanner scanner) {
		boolean enteredValidNumber = false;
		int gameSquareNumber = 0;
		while(!enteredValidNumber) {
			// Prompt the player to make a move
			System.out.println("To make a move, enter a number 1 through 9, corresponding to the square you'd like to take.");
			System.out.print("Player " + current.getGameIdentifier() + " make your move: ");
			
			// Read the move that the user entered, and check to make sure it's a valid input
			gameSquareNumber = scanner.nextInt();
			if(gameSquareNumber >= 1 && gameSquareNumber <= 9) {
				enteredValidNumber = true;
			} else {
				System.out.println("Error! Please enter a valid number, between 1 and 9");
			}
		}
		return gameSquareNumber;
	}
	
	/** Helper method, which converts a number 1 through 9 to a row value.
	 * 
	 * @param number
	 * @return
	 */
	private int getRowFromNumber(int number) {
		return (number - 1) / 3; // number is 1 indexed
	}
	
	/** Helper method, which converts a number 1 through 9 to a column value.
	 * 
	 * @param number
	 * @return
	 */
	private int getColFromNumber(int number) {
		return (number - 1) % 3; // number is 1 indexed
	}
	
	private void printBoardPosition() {
		String boardPosition = this.board.positionAsString();
		System.out.println(boardPosition);
	}
	
	private void printBoardAvailableSpots() {
		String position = this.board.availablePositionsAsString();
		System.out.println(position);
	}
}
