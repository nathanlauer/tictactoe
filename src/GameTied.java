/** Subclass of the abstract class GameResult, which represents a tie. As the
 * game resulted in a tie, there is no winning player and no losing player.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class GameTied extends GameResult {
	public GameTied() {
		// nothing to do, empty constructor
	}
	
	/** Always returns false, since a tied game by definition was not won.
	 * 
	 */
	public boolean wonByPlayer(Player player) {
		return false;
	}
	
	/** Always returns false, since a tied game by definition was not lost.
	 * 
	 */
	public boolean lostByPlayer(Player player) {
		return false;
	}
	
	/** Always returns true - a tied game was indeed tied.
	 * 
	 */
	public boolean tied() {
		return true;
	}
}
