import java.util.ArrayList;
import java.util.Scanner;

/** This class coordinates the process of running multiple games of TicTacToe. 
 * It  tracks the results of all the games, and then prints out a summary at the end.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class TicTacToeRunner implements GameRunner {
	private Player xPlayer;
	private Player oPlayer;
	private ArrayList<GameResult> gameResults;
	
	public TicTacToeRunner() {
		this.xPlayer = new Player("X");
		this.oPlayer = new Player("O");
		this.gameResults = new ArrayList<GameResult>();
	}
	
	/** The "main" method, which runs games of TicTacToe until the user
	 * indicates that they are finished. Once done, a summary is printed.
	 * 
	 */
	public void playGames() {
		// Play games until the user indicates that they are finished.
		boolean finished = false;
		Scanner scanner = new Scanner(System.in);
		while(!finished) {
			TicTacToe game = new TicTacToe(this.xPlayer, this.oPlayer, scanner);
			GameResult result = game.play();
			this.gameResults.add(result);
			
			scanner.nextLine(); // consume any new line characters left over
			System.out.println("Would you like to play again? (Y/y)\n");
			String input = scanner.nextLine();
			if(!input.equals("Y") && !input.equals("y")) {
				finished = true;
			} else {
				System.out.println("--------------------------------------");
				System.out.print("New game! - "); // the remainder of the message on this line will come from the TicTacToe class.
			}
		}
		scanner.close();
	}
	
	/** Prints a welcome message.
	 * 
	 */
	public void printWelcomeMessage() {
		String welcomeMessage = "Welcome to Tic-Tac-Toe!\n";
		welcomeMessage += "In this game, two players -- X and O -- iteratively take turns grabbing an available square. First player to get three in a row wins!\n";
		welcomeMessage += "You can play as many games as you'd like. Once finished, a summary of the total results will be printed out.\n";
		welcomeMessage += "-------";
		System.out.println(welcomeMessage);
	}
	
	/** For all the games played, prints out the results summary.
	 * 
	 */
	public void printSummary() {
		int xPlayerWins = 0;
		int xPlayerLosses = 0;
		int oPlayerWins = 0;
		int oPlayerLosses = 0;
		
		// Tally record, and print result of each game
		System.out.println("Finished. Game Summary:");
		for(int i = 0; i < this.gameResults.size(); i++) {
			GameResult result = this.gameResults.get(i);
			if(result instanceof GameTied) {
				System.out.println("Game " + i + ": Tied.");
			} else if(result instanceof GameWonByPlayer) {
				GameWonByPlayer wonResult = (GameWonByPlayer)result;
				if(wonResult.wonByPlayer(this.xPlayer)) {
					xPlayerWins++;
					oPlayerLosses++;
					System.out.println("Game " + i + ": Won by Player " + this.xPlayer.getGameIdentifier());
				} else {
					oPlayerWins++;
					xPlayerLosses++;
					System.out.println("Game " + i + ": Won by Player " + this.oPlayer.getGameIdentifier());
				}
			} else {
				System.out.println("Error! Unknown GameResult type");
			}
		}
		
		// Output final records:
		System.out.println("-- Final Records --");
		System.out.println("Player X: " + xPlayerWins + " wins, " + xPlayerLosses + " losses.");
		System.out.println("Player O: " + oPlayerWins + " wins, " + oPlayerLosses + " losses.");
		System.out.println("Game over");
	}
}
