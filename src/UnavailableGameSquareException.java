/** Class which represents an exception for situations when a GameSquare is
 * unavailable, yet some action was attempted. 
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
@SuppressWarnings("serial")
public class UnavailableGameSquareException extends Exception {

	public UnavailableGameSquareException (String message) {
		super(message);
	}
}
