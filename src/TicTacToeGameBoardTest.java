import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TicTacToeGameBoardTest {
	private TicTacToeGameBoard board;
	Player xPlayer;
	Player oPlayer;
	Player[] allPlayers;
	
	// A Test class is a class like any other, hence constructors!
	public TicTacToeGameBoardTest() {
		this.board = new TicTacToeGameBoard();
		this.xPlayer = new Player("X");
		this.oPlayer = new Player("O");
		this.allPlayers = new Player[] {xPlayer, oPlayer};
	}
	
	@BeforeEach
	public void clearBoard() {
		this.board.clearBoard();
	}
	
	@Test
	public void wonTopRow() {
		int row = 0;
		try {
			this.board.claimGameSquare(row, 0, this.xPlayer);
			this.board.claimGameSquare(row, 1, this.xPlayer);
			this.board.claimGameSquare(row, 2, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonMiddleRow() {
		int row = 1;
		try {
			this.board.claimGameSquare(row, 0, this.xPlayer);
			this.board.claimGameSquare(row, 1, this.xPlayer);
			this.board.claimGameSquare(row, 2, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonBottomRow() {
		int row = 2;
		try {
			this.board.claimGameSquare(row, 0, this.xPlayer);
			this.board.claimGameSquare(row, 1, this.xPlayer);
			this.board.claimGameSquare(row, 2, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonLeftColumn() {
		int col = 0;
		try {
			this.board.claimGameSquare(0, col, this.xPlayer);
			this.board.claimGameSquare(1, col, this.xPlayer);
			this.board.claimGameSquare(2, col, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonMiddleColumn() {
		int col = 1;
		try {
			this.board.claimGameSquare(0, col, this.xPlayer);
			this.board.claimGameSquare(1, col, this.xPlayer);
			this.board.claimGameSquare(2, col, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonRightColumn() {
		int col = 2;
		try {
			this.board.claimGameSquare(0, col, this.xPlayer);
			this.board.claimGameSquare(1, col, this.xPlayer);
			this.board.claimGameSquare(2, col, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonTopLeftToBottomRight() {
		try {
			this.board.claimGameSquare(0, 0, this.xPlayer);
			this.board.claimGameSquare(1, 1, this.xPlayer);
			this.board.claimGameSquare(2, 2, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void wonBottomLeftToTopRight() {
		try {
			this.board.claimGameSquare(2, 0, this.xPlayer);
			this.board.claimGameSquare(1, 1, this.xPlayer);
			this.board.claimGameSquare(0, 2, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertTrue(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void nonWinningPosition1() {
		try {
			this.board.claimGameSquare(0, 0, this.xPlayer);
			this.board.claimGameSquare(0, 1, this.xPlayer);
			this.board.claimGameSquare(1, 1, this.xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertFalse(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
	
	@Test
	public void nonWinningPosition2() {
		try {
			this.board.claimGameSquare(2, 0, xPlayer);
			this.board.claimGameSquare(0, 0, xPlayer);
			this.board.claimGameSquare(0, 2, xPlayer);
			this.board.claimGameSquare(2, 2, xPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertFalse(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertFalse(this.board.positionIsTied(this.allPlayers));
	}
		
	@Test
	public void tiedPosition() {
		try {
			this.board.claimGameSquare(0, 0, this.xPlayer);
			this.board.claimGameSquare(0, 1, this.oPlayer);
			this.board.claimGameSquare(0, 2, this.xPlayer);
			
			this.board.claimGameSquare(1, 0, this.xPlayer);
			this.board.claimGameSquare(1, 1, this.oPlayer);
			this.board.claimGameSquare(1, 2, this.xPlayer);
			
			this.board.claimGameSquare(2, 0, this.oPlayer);
			this.board.claimGameSquare(2, 1, this.xPlayer);
			this.board.claimGameSquare(2, 2, this.oPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown an index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown an UnavailableGameSquareException");
		}
		assertFalse(this.board.winningPositionForPlayer(this.xPlayer));
		assertFalse(this.board.winningPositionForPlayer(this.oPlayer));
		assertTrue(this.board.positionIsTied(this.allPlayers));
	}
}
