/** This interface defines a set of methods that all game board must implement.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public interface GameBoard {
	public String positionAsString();
	public String availablePositionsAsString();
	public boolean isGameSquareOccupied(int row, int col) throws IndexOutOfBoundsException;
	public void clearGameSquare(int row, int col) throws IndexOutOfBoundsException;
	public void claimGameSquare(int row, int col, Player player) throws UnavailableGameSquareException, IndexOutOfBoundsException;
	public void clearBoard();
	public boolean winningPositionForPlayer(Player player);
	public boolean positionIsTied(Player[] allPlayers);
}
