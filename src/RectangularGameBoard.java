/** Class which represents a Rectangular Game Board. This class is not the most generic type of GameBoard, 
 * as it makes a number of assumptions about the type of game being played. However, it is a good 
 * abstraction for any game which has a rectangular board, in which each square is either empty, 
 * or occupied by some player. Examples of these types of games include Tic Tac Toe, checkers, chess, go, etc.
 * 
 * This class provides a number of methods that can apply to any rectangular board game. These include:
 * - claimGameSquare: a Player can claim a certain square on the board. 
 * - printBoard: formats the style of the current position on the board, and returns it as a String
 * - isGameSquareOccupied: indicates whether or not a certain square is currently occupied by a player
 * - clearGameSquare: clears the square in a certain position.
 * 
 * All positions on the board are indexed by a (row, col) pair. Internally, the position of the board
 * starts at (0, 0), and increases monotonically until the point (rows - 1, cols - 1);
 * 
 * This class does not allow access to any of the GameSquares that collectively make up the board.
 * Access to these GameSquares would otherwise break encapsulation, as a client could do something
 * with the GameSquares without this class's knowledge.
 * 
 * Subclasses of this class are expected to implement any specifics to their game. 
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class RectangularGameBoard implements GameBoard {
	protected final int numRows;
	protected final int numCols;
	protected GameSquare[][] gameSquares;
	
	/** Standard constructor. Rows and cols here are nominal: meaning, if
	 * rows = 3, and cols = 4, then a board with 3 rows and 4 columns is created. 
	 * However, internally, rows and cols are zero indexed. Thus, if you attempt to
	 * access RectangularGameBoard(3, 4), that will throw an IndexOutOfBounds exception.
	 * 
	 * 
	 * @param rows
	 * @param cols
	 */
	public RectangularGameBoard(int rows, int cols) {
		// Do not allow negative values to be entered for rows and cols
		if(rows < 0) {
			throw new IndexOutOfBoundsException("Rows cannot be negative");
		}
		
		if(cols < 0) {
			throw new IndexOutOfBoundsException("Cols cannot be negative");
		}
		
		this.numRows = rows;
		this.numCols = cols;
		this.gameSquares = new GameSquare[this.numRows][this.numCols];
		this.initGameSquares();
	}
	
	/** Initializes the board with GameSquares at each (row, col) pair, from (0,0)
	 * to (row - 1, col - 1)
	 * 
	 */
	private void initGameSquares() {
		for(int row = 0; row < this.numRows; row++) {
			for(int col = 0; col < this.numCols; col++) {
				GameSquare gameSquare = new GameSquare(row, col);
				this.gameSquares[row][col] = gameSquare;
			}
		}
	}
	
	/** Claims the square at the indicated row and col position for the 
	 * passed in Player, if it's available. If it's not available, throws
	 * an exception.
	 * 
	 * row and col are zero indexed, so the valid range is (0, numRows - 1), (0, numCols - 1)
	 * 
	 * @param row
	 * @param col
	 * @param player
	 * @throws UnavailableGameSquareException, IndexOutOfBoundsException
	 */
	public void claimGameSquare(int row, int col, Player player) throws UnavailableGameSquareException, IndexOutOfBoundsException {
		if(this.invalidRowCol(row, col)) {
			throw new IndexOutOfBoundsException();
		}
		
		// check to see if this GameSquare is currently occupied
		if(this.isGameSquareOccupied(row, col)) {
			throw new UnavailableGameSquareException("Attempted to claim GameSquare that was previously occupied!");
		}
		
		// At this point, we can now assign this GameSquare to the passed in player
		GameSquare relevantGameSquare = this.gameSquares[row][col]; // reference copy
		relevantGameSquare.claim(player);
	}
	
	/** Formats the current position of the Board, and returns it as a multi-line string.
	 * For example, in the game Tic Tac Toe:
	 * +--+--+--+
	 * |O |O |  |
	 * +--+--+--+
	 * |X |  |  |
	 * +--+--+--+
	 * |  |  |  |
	 * +--+--+--+
	 * 
	 * @return
	 */
	public String positionAsString() {
		boolean showPlayersPositions = true;
		return this.boardPositionAsString(false, showPlayersPositions);
	}
	
	/** Formats the current position of the Board, and returns it as a multi-line string, 
	 * showing the available positions as numbers.
	 * For example, in the game Tic Tac Toe:
	 * +--+--+--+
	 * |  |  |3 |
	 * +--+--+--+
	 * |  |5 |6 |
	 * +--+--+--+
	 * |7 |8 |9 |
	 * +--+--+--+
	 * 
	 * @return
	 */
	public String availablePositionsAsString() {
		boolean numberedEmptySpots = true;
		return this.boardPositionAsString(numberedEmptySpots, false);
	}
	
	/** Private helper function which handles that actual conversion of the 
	 * board position into a string for the previous two methods.
	 * 
	 * @return
	 */
	private String boardPositionAsString(boolean numberedEmptySpots, boolean showPlayersPositions) {
		final String rowDelimeter = "+--+--+--+";
		final String colDelimeter = "|";
		final String newLine = "\n";
		String boardDisplay = "";
		
		for(int row = 0; row < this.numRows; row++) {
			// Every row starts with the rowDelimeter at the top
			boardDisplay += rowDelimeter;
			boardDisplay += newLine;
			
			// Now, for the columns, start with the colDelimeter
			boardDisplay += colDelimeter;
			for(int col = 0; col < this.numCols; col++) {
				// Check if this GameSquare is occupied
				GameSquare currentSquare = this.gameSquares[row][col];
				if(!currentSquare.isOccupied()) {
					if(numberedEmptySpots) {
						int squareNumber = row * this.numRows + col % this.numCols + 1; // 1-indexed
						boardDisplay += squareNumber + " ";
					} else {
						boardDisplay += "  ";
					}
				} else {
					if(showPlayersPositions) {
						// Get the gameIdentifier of the player that owns this GameSquare
						String gameIdentifier = currentSquare.onwersIdentifier();
						if(gameIdentifier.length() == 0) {
							boardDisplay += "  ";
						} else if(gameIdentifier.length() == 1) {
							boardDisplay += gameIdentifier + " ";
						} else if(gameIdentifier.length() == 2) {
							boardDisplay += gameIdentifier;
						} else {
							// Only print the first two spots for this Player
							boardDisplay += gameIdentifier.substring(0, 2);
						}
					} else {
						boardDisplay += "  ";
					}
				}
				
				// And append the next colDelimeter
				boardDisplay += colDelimeter;
			}
			boardDisplay += newLine;
		}
		
		// Finally, append one final rowDelimeter for the bottom.
		boardDisplay += rowDelimeter;
		boardDisplay += newLine;
		
		return boardDisplay;
	}
	
	/** Returns true if the GameSquare at position row, col is currently occupied.
	 *  Otherwise, returns false.
	 * 
	 * row and col are zero indexed, so the valid range is (0, numRows - 1), (0, numCols - 1)
	 * 
	 * @param row
	 * @param col
	 * @return
	 * @throws IndexOutOfBoundsException
	 */
	public boolean isGameSquareOccupied(int row, int col) throws IndexOutOfBoundsException {
		if(this.invalidRowCol(row, col)) {
			throw new IndexOutOfBoundsException();
		}
		
		GameSquare relevantGameSquare = this.gameSquares[row][col]; // reference copy
		return relevantGameSquare.isOccupied();
	}
	
	/** Clears the GameSquare at the (row, col) position. 
	 * row and col are zero indexed, so the valid range is (0, numRows - 1), (0, numCols - 1)
	 * 
	 * @param row
	 * @param col
	 * @throws IndexOutOfBoundsException
	 */
	public void clearGameSquare(int row, int col) throws IndexOutOfBoundsException {
		if(this.invalidRowCol(row, col)) {
			throw new IndexOutOfBoundsException();
		}
		
		GameSquare relevantGameSquare = this.gameSquares[row][col]; // reference copy
		relevantGameSquare.clear();
	}
	
	/** Clears every square on the board
	 * 
	 */
	public void clearBoard() {
		for(int row = 0; row < this.numRows; row++) {
			for(int col = 0; col < this.numCols; col++) {
				this.clearGameSquare(row, col);
			}
		}
	}
	
	/** Indicates whether or not the current position is a win for the passed in Player.
	 *  Should be overridden by a subclass.
	 * 
	 * @param player
	 * @return
	 */
	public boolean winningPositionForPlayer(Player player) {
		System.out.println("Function winningPositionForPlayer should be overridden by subclass!");
		return false; 
	}
	
	/** Indicates whether or not the current position results in a tied game.
	 *  Should be overridden by a subclass.
	 * 
	 * @return
	 */
	public boolean positionIsTied(Player[] allPlayers) {
		System.out.println("Function positionIsTieds should be overridden by subclass!");
		return false;
	}
	
	/** Returns true if all squares on the board are occupied
	 * 
	 * @return
	 */
	protected boolean allSquaresOccupied() {
		for(int row = 0; row < this.numRows; row++) {
			for(int col = 0; col < this.numCols; col++) {
				GameSquare current = this.gameSquares[row][col];
				if(!current.isOccupied()) {
					return false;
				}
			}
		}
		return true;
	}
	
	/** Indicates whether or not the passed in row and col pair are valid. 
	 *  Validity means that row and col are non-negative and less than
	 *  numRows and numCols.
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	protected boolean invalidRowCol(int row, int col) {
		return row >= this.numRows || row < 0 || col >= this.numCols || col < 0;
	}
}
