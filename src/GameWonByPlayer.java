/** Subclass of the abstract class GameResult, which represents a game
 * in which some player won, and some player lost.
 * 
 * Note that there are games with different semantics. For example, there
 * may be games with multiple winners, multiple losers, or one winner but
 * multiple losers, etc. That being said, this subclass specifically represents
 * a game with one winner and one loser. Should a client need to store the
 * results of a game with different semantics, a different subclass of GameResult
 * should be used.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class GameWonByPlayer extends GameResult {
	private Player winner;
	private Player loser;
	
	/** Constructor where the winning Player and losing Player are passed as arguments.
	 * 
	 * @param winningPlayer
	 * @param losingPlayer
	 */
	public GameWonByPlayer(Player winningPlayer, Player losingPlayer) {
		this.winner = winningPlayer;
		this.loser = losingPlayer;
	}
	
	/** Returns true if this game was won by the passed in Player.
	 * 
	 */
	public boolean wonByPlayer(Player player) {
		return this.winner.equals(player);
	}
	
	/** Returns true if this game was lost by the passed in Player.
	 * 
	 */
	public boolean lostByPlayer(Player player) {
		return this.loser.equals(player);
	}
	
	/** Always returns false - since this game was won by a certain player, by
	 * definition it was not tied.
	 * 
	 */
	public boolean tied() {
		return false;
	}
}
