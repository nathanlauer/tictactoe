/** This class is a subclass of the RectangularGameBoard, and contains the semantics
 * specific to the TicTacToe game. These include:
 * -A game board of size 3x3
 * -Specifics on when a position is won or tied
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class TicTacToeGameBoard extends RectangularGameBoard {	
	/** Standard constructor for a TicTacToeGameBoard. 
	 * 
	 */
	public TicTacToeGameBoard() {
		super(3, 3); //Create a RectangularGameBoard of size 3 by 3
	}
	
	/** Indicates whether or not the passed in Player has won the game
	 * 
	 */
	@Override
	public boolean winningPositionForPlayer(Player player) {
		// In Tic Tac Toe, there are 8 possible ways for a player to win:
		// Three across in each of the three rows
		// Three down in each of the three columns
		// Three diagonally on the two diagonals
		
		// Check each row
		for(int row = 0; row < this.numRows; row++) {
			if(this.gameSquares[row][0].ownedByPlayer(player) &&
			   this.gameSquares[row][1].ownedByPlayer(player) && 
			   this.gameSquares[row][2].ownedByPlayer(player)) {
				return true;
			}
		}
		
		// Check each column
		for(int col = 0; col < this.numCols; col++) {
			if(this.gameSquares[0][col].ownedByPlayer(player) &&
			   this.gameSquares[1][col].ownedByPlayer(player) && 
			   this.gameSquares[2][col].ownedByPlayer(player)) {
				return true;
			}
		}
		
		// Check top left to bottom right
		if(this.gameSquares[0][0].ownedByPlayer(player) &&
		   this.gameSquares[1][1].ownedByPlayer(player) && 
		   this.gameSquares[2][2].ownedByPlayer(player)) {
			return true;
		}
		
		// Check bottom left to top right
		if(this.gameSquares[2][0].ownedByPlayer(player) &&
		   this.gameSquares[1][1].ownedByPlayer(player) && 
		   this.gameSquares[0][2].ownedByPlayer(player)) {
			return true;
		}	
		
		return false;
	}
	
	/** Indicates whether or not the current position would result in a 
	 * forced tie. Note that if the game is not yet finished, without a 
	 * winner, this will still return false. This method only returns true
	 * once all squares have been occupied, and no winner has been found.
	 * 
	 */
	@Override
	public boolean positionIsTied(Player[] allPlayers) {
		// First, make sure all squares are occupied
		if(!this.allSquaresOccupied()) {
			return false; // can't have a tie when there are still moves to be made
		}
		
		// Next, make sure no player has won
		for(Player player : allPlayers) {
			if(winningPositionForPlayer(player)) { 
				return false; // not a tie, player won
			}
		}
		
		// If we've reached this point, then the position is drawn
		return true;
	}
}
