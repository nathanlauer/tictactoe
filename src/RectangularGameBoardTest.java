import static org.junit.Assert.*;

import org.junit.Test;

public class RectangularGameBoardTest {

	@Test
	public void emptyBoard() {
		int numRows = 3;
		int numCols = 3;
		RectangularGameBoard board = new RectangularGameBoard(numRows, numCols);
		
		// All game squares should be empty
		for(int row = 0; row < numRows; row++) {
			for(int col = 0; col < numCols; col++) {
				assertFalse(board.isGameSquareOccupied(row, col));
			}
		}
		
		// Game board should look like this:
		// +--+--+--+
		// |  |  |  |
		// +--+--+--+
		// |  |  |  |
		// +--+--+--+
		// |  |  |  |
		// +--+--+--+
		String emptyBoardDisplay = "+--+--+--+\n";
		emptyBoardDisplay += "|  |  |  |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|  |  |  |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|  |  |  |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		assertEquals(emptyBoardDisplay, board.positionAsString());
		
		// And repeat but with a numbered board
		emptyBoardDisplay = "+--+--+--+\n";
		emptyBoardDisplay += "|1 |2 |3 |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|4 |5 |6 |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|7 |8 |9 |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		assertEquals(emptyBoardDisplay, board.availablePositionsAsString());
	}

	@Test
	public void midGameBoard() {
		// In this test, we create a board with some Players having taken positions
		int numRows = 3;
		int numCols = 3;
		RectangularGameBoard board = new RectangularGameBoard(numRows, numCols);
		
		Player xPlayer = new Player("X");
		Player yPlayer = new Player("Y");
		Player zPlayer = new Player("Z");
		
		// Claim some spots to simulate the progression of the game
		try {
			board.claimGameSquare(1, 1, xPlayer);
			board.claimGameSquare(0, 1, xPlayer);
			board.claimGameSquare(0, 2, yPlayer);
			board.claimGameSquare(2, 1, zPlayer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			e.printStackTrace();
			fail("Should not have thrown square unavailable");
		}
		
		// Game board should look like this:
		String emptyBoardDisplay = "+--+--+--+\n";
		emptyBoardDisplay += "|  |X |Y |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|  |X |  |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|  |Z |  |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		assertEquals(emptyBoardDisplay, board.positionAsString());
		
		// Numbered board
		emptyBoardDisplay = "+--+--+--+\n";
		emptyBoardDisplay += "|1 |  |  |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|4 |  |6 |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		emptyBoardDisplay += "|7 |  |9 |\n";
		emptyBoardDisplay += "+--+--+--+\n";
		assertEquals(emptyBoardDisplay, board.availablePositionsAsString());
	}
	
	@Test
	public void exceptionOnSquarePreviouslyOccupied() {
		// In this test, we attempt to have one Player claim a spot that is already taken
		int numRows = 3;
		int numCols = 3;
		RectangularGameBoard board = new RectangularGameBoard(numRows, numCols);
		
		Player xPlayer = new Player("X");
		Player yPlayer = new Player("Y");
		
		try {
			board.claimGameSquare(2, 2, xPlayer);
			board.claimGameSquare(2, 2, yPlayer);
			
			fail("Should have thrown UnavailableSquareException");
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			fail("Should not have thrown index out of bounds exception");
		} catch (UnavailableGameSquareException e) {
			// Success!
			assertEquals(e.getMessage(), "Attempted to claim GameSquare that was previously occupied!");
		}
	}
}
