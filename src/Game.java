/** Interface for a game. Every game must implement the play method, which returns a GameResult.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public interface Game {
	public GameResult play();
}
