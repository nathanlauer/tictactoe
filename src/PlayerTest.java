import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void gameIdentifier() {
		String xIdentifier = "X";
		String oIdentifier = "O";
		
		Player xPlayer = new Player(xIdentifier);
		Player oPlayer = new Player(oIdentifier);
		
		// Ensure that each player is correctly identified by their gameIdentifier
		assertTrue(xPlayer.hasGameIdentifier(xIdentifier));
		assertFalse(xPlayer.hasGameIdentifier(oIdentifier));
		assertTrue(oPlayer.hasGameIdentifier(oIdentifier));
		assertFalse(oPlayer.hasGameIdentifier(xIdentifier));
	}
	
	@Test
	public void equality() {
		Player xPlayer = new Player("X");
		Player oPlayer = new Player("O");
		
		// Test for equality
		assertTrue(xPlayer.equals(xPlayer));
		assertTrue(oPlayer.equals(oPlayer));
		assertFalse(xPlayer.equals(oPlayer));
		assertFalse(oPlayer.equals(xPlayer));
	}
}
