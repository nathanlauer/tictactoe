import static org.junit.Assert.*;

import org.junit.Test;

public class GameResultTest {

	@Test
	public void gameTied() {
		GameTied tiedGame = new GameTied();
		Player xPlayer = new Player("X");
		
		// A tied game does not have a winner, or a loser
		assertFalse(tiedGame.wonByPlayer(xPlayer));
		assertFalse(tiedGame.lostByPlayer(xPlayer));
		assertTrue(tiedGame.tied());
	}

	@Test
	public void gameWon() {
		Player winner = new Player("X");
		Player loser = new Player("O");
		GameWonByPlayer game = new GameWonByPlayer(winner, loser);
		
		// Ensure that the result was properly stored
		assertTrue(game.wonByPlayer(winner));
		assertFalse(game.wonByPlayer(loser));
		assertTrue(game.lostByPlayer(loser));
		assertFalse(game.lostByPlayer(winner));
		assertFalse(game.tied());
	}
}
