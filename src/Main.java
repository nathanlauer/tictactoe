/** This class acts as the entry point for the game Tic Tac Toe. 
 *  It is simply an entry point to the program, and just contains a main method.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions.
 * 
 * I hope you are having a nice day!
 */
public class Main {
	/** Entry method to the Tic Tac Toe game.
	 * 
	 * @param args - command line arguments. At the moment, these are irrelevant to the game play.
	 */
	public static void main(String[] args) {
		TicTacToeRunner runner = new TicTacToeRunner();
		runner.printWelcomeMessage();
		runner.playGames();
		runner.printSummary();
	}

}
