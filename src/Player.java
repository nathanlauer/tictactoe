import java.util.*;

/** This class represents a player in a game. Every player has a unique ID, and an identifier which is used
 * to separate players within the semantics of a specific game. For example, in Tic Tac Toe, a player may have 
 * a gameIdentifier "O," which is specific to the semantics of the game Tic Tac Toe.
 * 
 * This class makes no assumptions about the nature of the game the player is playing. It is effectively a wrapper
 * around an ID and a game identifier.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions.
 * 
 * I hope you are having a nice day!
 *
 */
public class Player {
	private UUID uuid;
	private String gameIdentifier; 
	
	/** Constructor for a Player, specified by the passed in gameIdentifier.
	 * Internally, a globally unique UUID is assigned to this player.
	 * 
	 * @param gameIdentifier
	 */
	public Player(String gameIdentifier) {
		this.uuid = UUID.randomUUID();
		this.gameIdentifier = gameIdentifier;
	}
	
	/** Returns the UUID of this player.
	 *  
	 * @return the UUID of this player.
	 */
	public UUID getId() {
		return this.uuid;
	}
	
	/** Returns the gameIdentifier for this Player.
	 * 
	 * @return the gameIdentifier for this player.
	 */
	public String getGameIdentifier() {
		return this.gameIdentifier;
	}
	
	/** Indicates if this Player has the specified ID
	 * 
	 * @param id
	 * @return true if the passed id is equal to the UUID of this Player.
	 */
	public boolean hasUUID(UUID id) {
		return id.equals(this.uuid);
	}
	
	/** Indicates if this Player has the specified gameIdentifier
	 * 
	 * @param gameIdentifier
	 * @return true if the passed in gameIdentifier is equal to the gameIdentifier of this Player.
	 */
	public boolean hasGameIdentifier(String gameIdentifier) {
		return this.gameIdentifier.equals(gameIdentifier);
	}
	
	/** toString method for a Player. Returns String with the UUID and gameIdentifier
	 * 
	 */
	@Override
	public String toString() {
		return "Player with UUID: " + uuid.toString() + ", game identifier: " + this.gameIdentifier;
	}
	
	/** Indicates whether or not this Player is equal to the passed in Object other.
	 *  They are equal, if other is an instance of a Player, and they have the 
	 *  same UUID and the same game identifier.
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		if(!(other instanceof Player)) {
			return false; // Not a player, can't be the same
		}
		
		Player player = (Player)other; // cast to a Player object
		return this.hasUUID(player.getId()) && this.hasGameIdentifier(player.getGameIdentifier());
	}
}
