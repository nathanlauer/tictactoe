/** Interface for a GameRunner - a class which coordinates the process of executing a game multiple times. 
 *  Each GameRunner must provide a means to print a welcome message, play the games, and print a summary
 *  at the end.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public interface GameRunner {
	public void printWelcomeMessage();
	public void playGames();
	public void printSummary();
}
