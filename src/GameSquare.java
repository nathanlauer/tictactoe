/** Class that represents a single square within a GameBoard. A square can be empty -- meaning that no player
 * currently occupies this square, or occupied -- meaning that this square is owned by a certain player. 
 * Once a square has been occupied by a certain player, it must be cleared before it can be occupied by
 * a different player.
 * 
 * A game square is instantiated with a certain position - this, in effect, is the only assumption this
 * class makes about the game being played; namely, that this square is part of a game in which the positions
 * of each square are important. The position of a GameSquare is specified by a row and column.
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public class GameSquare {
	private int row;
	private int col;
	private boolean occupied;
	private Player owner;
	
	public GameSquare(int row, int col) {
		this.row = row;
		this.col =  col;
		this.occupied = false;
		this.owner = null; // when created, this square is not owner by any player
	}
	
	/** Returns the row position of this GameSquare
	 * 
	 * @return
	 */
	public int getRow() {
		return this.row;
	}
	
	/** Returns the col position of this GameSquare
	 * 
	 * @return
	 */
	public int getCol() {
		return this.col;
	}
	
	/** Indicates if this GameSquare is currently occupied.
	 * 
	 * @return
	 */
	public boolean isOccupied() {
		return this.occupied;
	}
	
	/** Returns true if no Player currently owns this GameSquare.
	 * 
	 * @return
	 */
	public boolean available() {
		return !this.isOccupied();
	}
	
	/** Indicates if this GameSquare is owned by the passed in player.
	 * 
	 * @param p
	 * @return true if the passed in Player is the Player occupying this square. False otherwise.
	 */
	public boolean ownedByPlayer(Player player) {
		if(!this.occupied) {
			return false; // can't be owned if this GameSquare is empty
		}
		return this.owner.equals(player);
	}
	
	/** Clears this GameSquare - removes any Player that had owned this GameSquare, and 
	 * marks this GameSquare as being unoccupied.
	 * 
	 */
	public void clear() {
		this.owner = null;
		this.occupied = false;
	}
	
	/** Attempts to claim this GameSquare for the passed in player. Throws an exception
	 * if this square is already occupied.
	 * 
	 * @param p - the Player who should become the owner of this GameSquare.s
	 * @throws UnavailableGameSquareException
	 */
	public void claim(Player player) throws UnavailableGameSquareException {
		// First, ensure that this GameSquare is available
		if(this.occupied) {
			throw new UnavailableGameSquareException("Attempted to claim GameSquare that was previously occupied!");
		}
		
		this.owner = player;
		this.occupied = true;
	}
	
	/** Returns the identifier of the player that owns this GameSquare. 
	 * If no player owns this game square, returns the empty string.
	 * 
	 * @return
	 */
	public String onwersIdentifier() {
		if(this.occupied) {
			return this.owner.getGameIdentifier();
		}
		return "";
	}
}
