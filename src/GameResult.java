/** This is an abstract class which specifies the behavior for any subclass that
 * stores the result of a game. Subclasses are free to extend the behavior, but
 * at the very least, they must be able to indicate if the game was tied, or
 * which Player won and which Player lost.
 * 
 * As this is an abstract class, an instance cannot be instantiated.
 *  
 * 
 * @author nathanlauer
 * @email lauern@bu.edu - Please feel free to ask me any questions!
 *
 * I hope you are having a nice day!
 *
 */
public abstract class GameResult {
	public abstract boolean wonByPlayer(Player player);
	public abstract boolean lostByPlayer(Player player);
	public abstract boolean tied();
}
